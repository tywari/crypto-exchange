<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Libraries\User;
use App\Libraries\Trade;
use Validator;


class TradeController extends Controller {

    public function __construct(Request $request)
	{
        $this->middleware('group');
        
        $this->token = $request->session()->get('token');
        $this->user_id = User::get_user_id($this->token);

        

        $this->avalaible_coin = Trade::getAvailableCoins();
        //$this->active_coin_pair = ['inr-xrp'=>0,'inr-btc'=>0,'inr-eth'=>0,'inr-bch'=>0];
        $this->active_coin_pair = ['inr-xrp'=>0,'inr-btc'=>0,'inr-eth'=>0,'inr-bch'=>0, 'inr-ltc'=>0];
    }
    public function index($coin)
	{
        $s = User::checkKYC(['user_id'=>$this->user_id]);
        
        if(!$s)
            return redirect('/profile');
        $f = 1;
        
        if($coin!='')
        {
            $c = explode('-', $coin);
            if(isset($c[1]))
            {
                if((in_array($c[1], $this->avalaible_coin) && in_array($c[0], $this->avalaible_coin)) && $c[0]!=$c[1])
                {
                    $this->active_coin_pair[$coin] = 1;
                    $data=[
                        'from_currency' => $c[0],
                        'to_currency' => $c[1],
                        'active_coin_pair' =>$this->active_coin_pair,
                        'can_trade'=>$s
                    ];
                    return view('xchange/trade', $data);
                }
                else
                    $f = 0;

            }
            else
                $f = 0;
        }
        else
            $f = 0;
        
        if($f==0)
        {
            echo 'Invalid parameters';
            die;
        }
    }

    
}