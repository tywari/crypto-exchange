<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Traits\StatusResponse;
use App\Libraries\User;
use App\Libraries\Trade;
use App\Libraries\LogEvent;
use Validator;


class XchangeController extends Controller {

    use StatusResponse;
    
    public function __construct(Request $request)
	{
        $this->middleware('group');
        $this->token = $request->session()->get('token');
        $this->user_id = User::get_user_id($this->token);

        $this->user = new User();

        $this->buy_fee = 0;
        $this->min_buy_coin = [
            'xrp'=>1,
            'eth'=>'.001',
            'btc'=>'.0001',
            'bch'=>'.001',
            'ltc'=>'.001'
        ]; 
        
        $this->min_sell_coin = [
            'xrp'=>1,
            'eth'=>'.001',
            'btc'=>'.0001',
            'bch'=>'.001',
            'ltc'=>'.001'
        ]; 

        $this->fee = [
            'xrp'=>['buy'=>0, 'sell'=>0],
            'eth'=>['buy'=>0, 'sell'=>0],
            'btc'=>['buy'=>0, 'sell'=>0],
            'bch'=>['buy'=>0, 'sell'=>0],
            'ltc'=>['buy'=>0, 'sell'=>0],
        ];

        
    }

    // Buy coin
    public function buy(Request $request)
    {

        

        $validator = Validator::make($request->all(), $this->buySellRules(), $this->buySellValidationMessages());
        
        if ($validator->fails()) {
            
            $data = $validator->getMessageBag()->toArray();
            $message = $validator->errors()->first();
            return $this->_status('RPI', NULL, $data);

        } else {
            
            $this->from_currency = strtolower(trim($request->input('from_coin')));
            $this->to_currency = strtolower(trim($request->input('to_coin')));

            $vol  = trim($request->input('vol')); 
            $rate  = trim($request->input('at')); 

            // Check requested coin exist or not
            if(!array_key_exists($this->to_currency, $this->min_buy_coin))
                return $this->_status('INV');
            
            // Check mininum quantity of coin
            if($this->min_buy_coin[$this->to_currency] > $vol)
                return $this->_status('CST', 'Minimum order of '.$this->to_currency. ' is '.$this->min_buy_coin[$this->to_currency]);
            
            // Get current coin balance
            $s = User::balance($this->from_currency, $this->user_id);
            
            if($s['status'])
            {
                $total_amt = $vol*$rate;
                $bal = $s['data']['balance'];
                
                // Check sufficient balance for bid
                if($bal >= $total_amt)
                {
                    $fee = $this->fee[$this->to_currency]['buy'];
                    $d = [
                        'total_amt'=>$total_amt,
                        'tradable_amt'=>($total_amt - $fee),
                        'fee'=>$fee,
                        'rate'=>$rate,
                        'coin'=>$this->to_currency,
                        'volume'=>$vol,
                        'need_confirm'=>1
                    ];

                    $confirm_buy  = $request->input('confirm_buy'); 
                    if(!$confirm_buy)
                    {
                       return $this->_status('SUCC', 'Go to confirmation form user', $d);
                    }
                    else{

                        $d['balance'] = $bal;   // Add current balance into array too
                        $resp = $this->trade_buy(['type'=>'buy','data'=>$d]);
                        echo json_encode($resp);
                    }
                }
                else{
                    return $this->_status('CST', 'You don\'t have sufficient balance.');
                }
            }
            else{
                return $this->_status('CST', 'Can\'t get balance for coin '.$this->from_currency);
            }
        }
    }
    private function trade_buy($d)
    {
        $data = $d['data'];
        
        if($d['type']=='buy')
        {
            $bal = $data['balance']-$data['total_amt'];
            // Lock buy amount
            $s = User::lockCurrency(['coin'=>$this->from_currency, 'locked_bal'=>$data['total_amt'], 'balance'=>$bal, 'user_id'=>$this->user_id]);
            
            if(!$s['status'])
            {
                return $this->_status('CST', $s['message']);
            }
            else{
                //Add into log 
                $eventData = [
                    'user_id' => $this->user_id,
                    'user_ip' => \Request::ip(),
                    'event' => 'BUY '.strtoupper($this->to_currency),
                    'Data' =>  json_encode($d)
                ];
                $addEvt = LogEvent::addEvent($eventData);

                // Check ask into aks table for this bid request 
                $s = Trade::checkAsks(['rate'=>$data['rate'], 'from_coin'=>$this->from_currency, 'to_coin'=>$this->to_currency]);
                
                if($s['status'])
                {
                    $d = [
                        'from_currency'=>$this->from_currency,
                        'to_currency'=>$this->to_currency,
                        'fee'=>$data['fee'],
                        'rate'=>$data['rate'],
                        'volume'=>$data['volume'],
                        'user_id'=>$this->user_id
                    ];

                    if(empty($s['data'])) // If bid rate not found in ask table
                    {
                        $s = Trade::bid($d);

                        // $data = [
                        //     'from_currency'=>$this->from_currency,
                        //     'to_currency'=>$this->to_currency,
                        //     'trade'=>0,
                        //     'sell'=>0,
                        //     'buy'=>1
                        // ];
                        // $this->user->callNodeServer($data, 'all'); 


                       if($s['status'])
                       {
                            return $this->_status('SUCC', 'Bid has been placed.');
                       }
                       else{
                            return $this->_status('CST','Some technical issue raised.');
                       }
                    }
                    else{

                        $asks = $s['data'];
                        $vol  = $data['volume'];
                        $rate = $data['rate'];
                        $fee = $data['fee'];
                        
                        $remaining_vol = $vol;
                        foreach($asks as $k=>$ask)
                        {
                            
                            $extra_profit = 0;

                            $mode = '';
                            //Important :- Condition if sell volume less or equal or greater to bid volume
                            if($vol == $ask->volume){
                                
                                $effective_vol = $vol;
                                $mode = 'delete';
                            }
                            else
                            if($vol < $ask->volume){
                                $effective_vol = $vol;
                                $mode = 'minus';
                            }
                            else
                            {
                                $mode = 'delete';
                                $effective_vol = $ask->volume;
                            }

                            if($mode=='delete')
                                $s = Trade::deductAskVolume(['id'=>$ask->id, 'volume'=>$effective_vol, 'mode'=>'delete']);
                            else
                                $s = Trade::deductAskVolume(['id'=>$ask->id, 'volume'=>$effective_vol, 'mode'=>'minus']);


                            $extra_profit = ($rate*$effective_vol)-($ask->rate*$effective_vol); 
                            //  Maintain Bidder account
                            $deduct_locked_amt = $effective_vol;

                            //Deduct locked amount from akser account 
                            $s = User::deductLockedAmount(['locked_bal'=>$deduct_locked_amt, 'user_id'=>$ask->user_id, 'coin'=>$this->to_currency]);
                            
                            if($s['status'])
                            {
                                //Add asks currency to akser account wallet
                                $s = User::AddCurrencyAmount(['balance'=>($effective_vol*$ask->rate), 'user_id'=>$ask->user_id, 'coin'=>$this->from_currency]);
                            }
                            // End of maintain akser account 
                                
                            //  Maintain Bider account
                                $deduct_locked_amt = $effective_vol*$rate;

                                //Deduct locked amount from bider account 
                                $s = User::deductLockedAmount(['locked_bal'=>$deduct_locked_amt, 'user_id'=>$this->user_id, 'coin'=>$this->from_currency]);
                                
                                if($s['status'])
                                {
                                    $balance = $effective_vol*$rate;
                                    //Add asks currency to Bider account wallet
                                    $balance = $effective_vol;
                                    $s = User::AddCurrencyAmount(['balance'=>$balance, 'user_id'=>$this->user_id, 'coin'=>$this->to_currency]);
                                }
                            // End of maintain Bider account
                            
                            $td = [
                                'buyer_id'=>$this->user_id,
                                'seller_id'=>$ask->user_id,
                                'from_currency'=>$this->from_currency,
                                'to_currency'=>$this->to_currency,
                                'trade_type'=>'BUY',
                                'buyer_rate'=>$rate,
                                'seller_rate'=>$ask->rate,
                                'volume'=>$effective_vol,
                                'buy_fee'=>$fee,
                                'sell_fee'=>$ask->fee,
                                'trade_at'=>date('Y-m-d H:i:s'),
                                'extra_margin'=>$extra_profit
                            ];
                            
                            Trade::tradeEvent($td);

                            // // Call Node for broadcost trade
                            
                            // $data = [
                            //     'from_currency'=>$this->from_currency,
                            //     'to_currency'=>$this->to_currency,
                            //     'trade'=>1,
                            //     'sell'=>0,
                            //     'buy'=>0
                            // ];
                            // $this->user->callNodeServer($data, 'all'); 



                            $remaining_vol -= $effective_vol;


                            // Insert price into ticker
                            $pair = $this->from_currency.'/'.$this->to_currency;
                                            
                            $t = [
                                'where'=>['pair_coin'=>$pair],
                                'data'=>['pair_coin'=>$pair, 'rate'=>$rate, 'update_at'=>date('Y-m-d H:i:s')],
                            ];
                            Trade::updateTiker($t);


                            if($vol <= $effective_vol)
                                break;
                            
                        }

                        if($remaining_vol > 0)
                        {
                            $d = [
                                'from_currency'=>$this->from_currency,
                                'to_currency'=>$this->to_currency,
                                'fee'=>$data['fee'],
                                'rate'=>$data['rate'],
                                'volume'=>$remaining_vol,
                                'user_id'=>$this->user_id
                            ];
                            
                            $s = Trade::bid($d);

                            // $data = [
                            //     'from_currency'=>$this->from_currency,
                            //     'to_currency'=>$this->to_currency,
                            //     'trade'=>0,
                            //     'sell'=>0,
                            //     'buy'=>1
                            // ];
                            // $this->user->callNodeServer($data, 'all'); 



                            if($s['status'])
                            {
                                return $this->_status('SUCC', 'Bid has been placed.');
                            }
                            else{
                                return $this->_status('CST','Some technical issue raised.');
                            }
                        }

                        return $this->_status('SUCC', 'Bid has been placed.');

                    }
                }
                else{
                    return $this->_status('CST', $s['message']);
                }
            }   
        }
    }
    
    //sell coin
    public function sell(Request $request)
    {
       
        
        $validator = Validator::make($request->all(), $this->buySellRules(), $this->buySellValidationMessages());
        
        if ($validator->fails()) {
            
            $data = $validator->getMessageBag()->toArray();
            $message = $validator->errors()->first();
            return $this->_status('RPI', NULL, $data);

        } else {
            
            
            $this->from_currency = strtolower(trim($request->input('from_coin')));
            $this->to_currency = strtolower(trim($request->input('to_coin')));

            //$coin = strtolower(trim($request->input('coin'))); 
            $vol  = trim($request->input('vol')); 
            $rate  = trim($request->input('at')); 

    
            // Check requested coin exist or not
            if(!array_key_exists($this->from_currency, $this->min_sell_coin))
                return $this->_status('INV');
            
            // Check minimum quantity of coin
            if($this->min_sell_coin[$this->from_currency] > $vol)
                return $this->_status('CST', 'Minimum sell of '.$this->from_currency. ' is '.$this->min_sell_coin[$this->from_currency]);
            
            
            // Get current coin balance
            $s = User::balance($this->from_currency, $this->user_id);
            
            if($s['status'])
            {
                $total_amt = $vol*$rate;
                $bal = $s['data']['balance'];
                
                // Check sufficient balance for bid
                if($bal >= $vol)
                {
                    $fee = $this->fee[$this->from_currency]['sell'];
                    $d = [
                        'total_amt'=>$total_amt,
                        'tradable_amt'=>($total_amt - $fee),
                        'fee'=>$fee,
                        'rate'=>$rate,
                        'coin'=>$this->from_currency,
                        'volume'=>$vol,
                        'need_confirm'=>1
                    ];

                    $confirm_sell  = $request->input('confirm_sell'); 
                    if(!$confirm_sell)
                    {
                       return $this->_status('SUCC', 'Go to confirmation form user', $d);
                    }
                    else{
                        
                        $d['balance'] = $bal;   // Add current balance into array too
                        $resp = $this->trade_sell(['type'=>'sell','data'=>$d]);
                        echo json_encode($resp);
                    }
                }
                else{
                    return $this->_status('CST', 'You don\'t have sufficient balance.');
                }
            }
            else{
                return $this->_status('CST', 'Can\'t get balance for coin '.$this->from_currency);
            }
        }
    }
    private function trade_sell($d)
    {
        $data = $d['data'];
        
        if($d['type']=='sell')
        {
            $bal = $data['balance']-$data['volume'];
            // Lock buy amount
            $s = User::lockCurrency(['coin'=>$this->from_currency, 'locked_bal'=>$data['volume'], 'balance'=>$bal, 'user_id'=>$this->user_id]);
            
            if(!$s['status'])
            {
                return $this->_status('CST', $s['message']);
            }
            else{
                //Add into log 
                $eventData = [
                    'user_id' => $this->user_id,
                    'user_ip' => \Request::ip(),
                    'event' => 'SELL '.strtoupper($data['coin']),
                    'Data' =>  json_encode($d)
                ];
                $addEvt = LogEvent::addEvent($eventData);
                
                // Check ask into aks table for this bid request 
                $s = Trade::checkBids(['rate'=>$data['rate'], 'from_coin'=>$this->from_currency, 'to_coin'=>$this->to_currency]);
                
                if($s['status'])
                {
                    $d = [
                        'from_currency'=>$this->from_currency,
                        'to_currency'=>$this->to_currency,
                        'fee'=>$data['fee'],
                        'rate'=>$data['rate'],
                        'volume'=>$data['volume'],
                        'user_id'=>$this->user_id
                    ];

                    if(empty($s['data'])) // If ask rate not found in bid table
                    {
                        $s = Trade::ask($d);

                        // $data = [
                        //     'from_currency'=>$this->from_currency,
                        //     'to_currency'=>$this->to_currency,
                        //     'trade'=>0,
                        //     'sell'=>1,
                        //     'buy'=>0
                        // ];
                        // $this->user->callNodeServer($data, 'all');


                        if($s['status'])
                        {
                            return $this->_status('SUCC', 'Ask has been placed.');
                        }
                        else{
                            return $this->_status('CST','Some technical issue raised.');
                        }
                    }
                    else{   // If ask rate found in bid table
                        
                        $bids = $s['data'];
                        $vol  = $data['volume'];
                        $rate = $data['rate'];
                        $fee = $data['fee'];
                        $remaining_vol = $vol;
                        foreach($bids as $k=>$bid)
                        {
                           
                            $extra_profit = 0;
                            //Important :- Condition if sell volume less or equal or greater to bid volume
                            if($vol == $bid->volume){
                                
                                //Deduct volume from bid table 
                                $effective_vol = $vol;
                                $mode = 'delete';
                            }
                            else
                            if($vol < $bid->volume){
                                //Deduct volume from bid table 
                               $effective_vol = $vol;
                                $mode = 'minus';
                            }
                            else
                            {
                                //Deduct volume from bid table 
                                $effective_vol = $bid->volume;
                                $mode = 'delete';
                            }

                            if($mode=='delete')
                                $s = Trade::deductBidVolume(['id'=>$bid->id, 'volume'=>$effective_vol, 'mode'=>'delete']);
                            else
                                $s = Trade::deductBidVolume(['id'=>$bid->id, 'volume'=>$effective_vol, 'mode'=>'minus']);

                            $extra_profit = ($bid->rate*$effective_vol)-($rate*$effective_vol); 
                            

                            //  Maintain Bidder account
                                $deduct_locked_amt = $effective_vol*$bid->rate;
                                
                                //Deduct locked amount from bider account 
                                $s = User::deductLockedAmount(['locked_bal'=>$bid->rate*$effective_vol, 'user_id'=>$bid->user_id, 'coin'=>$this->to_currency]);
                                
                                if($s['status'])
                                {
                                    //Add asks currency to bidder account wallet
                                    $s = User::AddCurrencyAmount(['balance'=>$effective_vol, 'user_id'=>$bid->user_id, 'coin'=>$this->from_currency]);
                                }
                                
                            // End of maintain bidder account 
                                
                            //  Maintain Seller account
                                $deduct_locked_amt = $effective_vol;

                                //Deduct locked amount from seller account 
                                $s = User::deductLockedAmount(['locked_bal'=>$deduct_locked_amt, 'user_id'=>$this->user_id, 'coin'=>$this->from_currency]);
                                
                                if($s['status'])
                                {
                                    //Add asks currency to seller account wallet
                                    $balance = $effective_vol*$rate;
                                    $s = User::AddCurrencyAmount(['balance'=>$balance, 'user_id'=>$this->user_id, 'coin'=>$this->to_currency]);
                                }
                            // End of maintain Seller account


                            $td = [
                                'buyer_id'=>$bid->user_id,
                                'seller_id'=>$this->user_id,
                                'from_currency'=>$this->from_currency,
                                'to_currency'=>$this->to_currency,
                                'trade_type'=>'SELL',
                                'seller_rate'=>$rate,
                                'buyer_rate'=>$bid->rate,
                                'volume'=>$effective_vol,
                                'sell_fee'=>$fee,
                                'buy_fee'=>$bid->fee,
                                'trade_at'=>date('Y-m-d H:i:s'),
                                'extra_margin'=>$extra_profit
                            ];
                            Trade::tradeEvent($td);


                            // $data = [
                            //     'from_currency'=>$this->from_currency,
                            //     'to_currency'=>$this->to_currency,
                            //     'trade'=>1,
                            //     'sell'=>0,
                            //     'buy'=>0
                            // ];
                            // $this->user->callNodeServer($data, 'all');


                            $remaining_vol -= $effective_vol;


                            // Insert price into ticker
                            $pair = $this->to_currency.'/'.$this->from_currency;
                                            
                            $t = [
                                'where'=>['pair_coin'=>$pair],
                                'data'=>['pair_coin'=>$pair, 'rate'=>$rate, 'update_at'=>date('Y-m-d H:i:s')],
                            ];
                            Trade::updateTiker($t);


                            if($vol <= $effective_vol)
                                break;
                            
                        }

                        if($remaining_vol > 0)
                        {
                            $d = [
                                'from_currency'=>$this->from_currency,
                                'to_currency'=>$this->to_currency,
                                'fee'=>$data['fee'],
                                'rate'=>$data['rate'],
                                'volume'=>$remaining_vol,
                                'user_id'=>$this->user_id
                            ];

                            $s = Trade::ask($d);


                            // $data = [
                            //     'from_currency'=>$this->from_currency,
                            //     'to_currency'=>$this->to_currency,
                            //     'trade'=>0,
                            //     'sell'=>1,
                            //     'buy'=>0
                            // ];
                            // $this->user->callNodeServer($data, 'all');


                            if($s['status'])
                            {
                                return $this->_status('SUCC', 'Ask has been placed.');
                            }
                            else{
                                return $this->_status('CST','Some technical issue raised.');
                            }
                        }

                        return $this->_status('SUCC', 'Ask has been placed.');

                    }
                }
                else{
                    return $this->_status('CST', $s['message']);
                }
            }   
        }
    }

    // Get balance
    public function getCurrentBal(Request $request){
        
        $b=[];
        if(!empty($request->input('c')))
        {
            
            foreach($request->input('c') as $k=>$v)
            {
                $s = User::balance($v, $this->user_id);
                if($s['status'])
                {
                    $b[$v] = $s['data']['balance'];
                }
            }
        }
        return $this->_status('SUCC', '', $b);
    }

    // Get open orders
    public function get_open_orders(Request $request)
    {
        $from_currency =  strtolower($request->input('from_currency'));
        $to_currency =  strtolower($request->input('to_currency'));
        $trade_request = [
            'bid'=> [],
            'ask'=>[]
        ];
        
        $s = User::userBidsNAsks(['from_coin'=>$from_currency, 'to_coin'=>$to_currency, 'user_id'=>$this->user_id], 'bid');

        if($s['status'])
        {
            $trade_request['bid'] = $s['data'];
        }
        
        $s = User::userBidsNAsks(['from_coin'=>$to_currency, 'to_coin'=>$from_currency, 'user_id'=>$this->user_id], 'ask');
        if($s['status'])
        {
            $trade_request['ask'] = $s['data'];
        }

        return $this->_status('SUCC', '', $trade_request);
       
    }   

    public function cancel_trade(Request $request)
    {
        if(($request->input('row_id')==NULL || $request->input('row_id')=='') || ($request->input('trade_type')==NULL || $request->input('trade_type')==''))
            return $this->_status('CST', 'missing parameters');

        $trade_type = $request->input('trade_type');
        $row_id = $request->input('row_id');
        
        if($trade_type=='ask')
            $data['table'] = 'ask';
        else
            $data['table'] = 'bid';

        $data['where'] = ['id'=>$row_id];
        $data['select'] = ['id', 'user_id', 'volume', 'from_currency', 'fee', 'rate'];
        
        // Unlocked currency 
        $d = Trade::getRow($data);

        if(!empty($d)){

            if($d->user_id==$this->user_id)
            {
                $user_id = $d->user_id;
                if($d->from_currency!='inr')
                    $locked_amt = $d->volume;
                else
                    $locked_amt = $d->volume*$d->rate;

                $da=[
                    'user_id'=>$user_id,
                    'locked_bal'=>$locked_amt,
                    'coin'=>$d->from_currency,
                    'balance'=>$locked_amt
                ];
                $s = User::unLockCurrency($da);

                // Delete row from table
                if($trade_type=='ask')
                    $resp = Trade::deductAskVolume(['mode'=>'delete', 'id'=>$row_id]);
                else
                    $resp = Trade::deductBidVolume(['mode'=>'delete', 'id'=>$row_id]);

                //Add into log 
                $eventData = [
                    'user_id' => $this->user_id,
                    'user_ip' => \Request::ip(),
                    'event' => 'Cancelled '.$trade_type,
                    'Data' =>  json_encode($d)
                ];
                $addEvt = LogEvent::addEvent($eventData);

                return $this->_status('SUCC', 'Trade deleted successfully.');
            }
            else{
                return $this->_status('CST', 'You are not authorized to delete trade.');
            }
        }
        else{
            return $this->_status('CST', 'Not found in our record.');
        }
           
        
        
    }
    protected function buySellRules()
    {
        return [
            'to_coin' => 'required|max:3|min:3',
            'from_coin' => 'required|max:3|min:3',
            'vol' => 'required|numeric',
            'at' => 'required|numeric',
        ];
    }

    protected function buySellValidationMessages()
    {
        return [
            'to_coin.required' => 'To Coin should not be blank',
            'from_coin.required' => 'From Coin should not be blank',
            'vol.required' => 'Enter volume of coin',
            'at.required' => 'Rate should not be blank',
        ];
    }


    public function getTrades(Request $request)
    {
        $data['from_currency'] =  strtolower($request->input('from_currency'));
        $data['to_currency'] =  strtolower($request->input('to_currency'));
        
        $trades = Trade::getTrades($data);
        return $this->_status('SUCC', '', $trades);
    }

    public function ticker()
    {
        $ticker = Trade::ticker();
        return $this->_status('SUCC', '', $ticker);
    }

    public function buySaleOrder(Request $request)
    {
        $from_currency =  strtolower($request->input('from_currency'));
        $to_currency =  strtolower($request->input('to_currency'));
        $trade =  strtolower($request->input('trade'));
        $sell =  strtolower($request->input('sell'));
        $buy =  strtolower($request->input('buy'));

        $data = [
            'from_currency'=>$from_currency,
            'to_currency'=>$to_currency,
            'trade'=>$trade,
            'sell'=>$sell,
            'buy'=>$buy
        ];
        $this->user->callNodeServer($data, 'all');
    }


    public function mytrades(Request $request)
    {
        $data['from_currency'] =  strtolower($request->input('from_currency'));
        $data['to_currency'] =  strtolower($request->input('to_currency'));
        $data['user_id'] = $this->user_id;
        $trades = Trade::getTrades($data);
        return $this->_status('SUCC', '', $trades);
    }

    public function transactoin_history()
    {
        $transactions = Trade::transactoin_history($this->user_id);
        return $this->_status('SUCC', '', $transactions);
    }


    


}