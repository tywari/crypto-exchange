<html>
<head>
    <title>Team Giltxchange</title>
    <style type="text/css">
        .pad50{padding-left:50px;text-align: left}
        .pad25{padding-left:25px;}
    </style>
</head>
<body>
Dear {{$first_name}},
<br/><br/>
Kindly click on below link to reset your password. 
<br/>
<br/>
<a href="{{$forgot_link}}">Click here to reset password.</a>

<br/>
<br/>
<br/><br/><br/><br/><br/><br/>

Regards, Team Giltxchange
</body>
</html>
