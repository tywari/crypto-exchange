@extends('home.master')
@section('headerjs')
@endsection()

@section('content')
<header class="header-margin">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-10 header-center">
					<div class="headerform-main">
						<div class="header-content">
							<h2 class="blk-title">Buy, Sell, & Trade Bitcoin</h2>
							<div class="discription">
								More than 100 psd files including hundreds of sections. <span>BONUS: 2 ICO Landing Page,
								Blockfolio App & Cryptocurrency Magazine</span>
							</div>
							<div class="discover-btn">
								<button type="button" class="btn btn-global-dark">discover ICO <span><img src="{{asset('/public/gilt/img/btn-arrow.png')}}"/></span></button>
							</div>
						</div>
						<div class="header-form">
							<section class="login-form">
								<div class="form-wrap">
									<h1>Login into your account</h1>
									<form role="form" name="login-form" id="login-form" method="post" action="" autocomplete="off">
										<div class="form-group">											
											<input type="text" class="form-control" name="email" id="email" placeholder="Email address">
										</div>
										<div class="form-group">											
											<input type="password" class="form-control" id="password" name="password" placeholder="Password">
										</div>
										<div class="rebember-group">
											<div class="form-check">
												<input type="checkbox" class="filled-in form-check-input" id="checkbox101" checked="checked">
												<label class="form-check-label" for="checkbox101">Remember me</label>
											</div>
											<div class="forgot-pass">
												<a href="#" data-toggle="modal" data-target="#myModal">Forgot password?</a>
											</div>
										</div>
										<div class="login-btn">
											<button type="submit" class="btn btn-global">LOG IN <span><img src="{{asset('/public/gilt/img/btn-arrow.png')}}"/></span></button>
										</div>
										<div class="discrip">
											Lorem ipsum dolor sit amet, consecte tur adi pisc ing elit. Donec ultrices sus cipit element Fusce ut scelerisque urna, quis dapibus.
										</div>										
									</form>
								</div>
								<!-- Forgot Model Popup Box start-->											
								<div id="myModal" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title">Forgot Password</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<div class="modal-body">
												<form class="form" name="model-login-form" id="model-login-form" method="post" action="">
												<fieldset>
													<div class="form-group">
													<div class="input-group">
														<span class="input-group-addon"><i class="far fa-envelope"></i></span>			  
														<input id="emailInput" placeholder="email address" class="form-control" type="email">
													</div>
													</div>
													<div class="form-group">
													<button class="btn btn-global btn-block">SEND <span><img src="{{asset('/public/gilt/img/btn-arrow.png')}}"/></span></button>
													</div>
												</fieldset>
												</form>
										</div>
										<div class="modal-footers"></div>
									</div>
									</div>
								</div>
								<!-- Forgot Model Popup Box end-->
								<div class="login-footers">
									Want a new account?<a href="#" data-toggle="modal" data-target="#signup">Sign up now!</a>
								</div>								
								<!-- Sign Up Model Popup Box start-->											
								<div id="signup" class="modal fade" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title">Forgot Password</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<div class="modal-body">
											<form class="login-form signup-form" name="reg-form" id="reg-form" method="post" action="">
												<div class="form-group">					 													  
													<input type="text" class="form-control" name="firstname" placeholder="Firstname">
												</div>
												<div class="form-group">					 													  
													<input type="text" name="lastname" class="form-control" placeholder="Lastname">
												</div>			
												<div class="form-group">					 													  
													<input type="email" name="email" class="form-control" placeholder="Email Address">
												</div>
												<div class="form-group">					 													  
													<input type="text" name="username" class="form-control" placeholder="Username">
												</div>
												<div class="form-group">					 													  
													<input type="password" name="password" class="form-control" placeholder="password">
												</div>												
												<label><input type="checkbox" name="terms"> I agree with the <a href="#">Terms and Conditions</a>.</label>													
											  <br/><br/>
											  <div class="form-group">
												<button class="btn btn-global btn-block">SIGN UP <span><img src="{{asset('/public/gilt/img/btn-arrow.png')}}"/></span></button>
											  </div>												
											</form>
										</div>
										<div class="modal-footers"></div>
									</div>
									</div>
								</div>
								<!-- Sign-up Model Popup Box end-->
							</section>
						</div>						
					</div>        
				</div>
			</div>
		</div>
	 
   
    </header>
    <!-- Page Content -->
    <section class="bestbit-2">
      <div class="container">
			<div class="title">if you are wondering</div>
			<h3 class="sec-title">The Best Bitcoin Exchange</h3>
			<div class="discrip">
				Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam necodio vestibul. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blan dit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mattis effic iturut magna. Pelle ntesque sit am et tellus blandit. Etiam necodio vestibul. Pellentesque sit am et tellus blandit. Etiam necodio vestibul. Etiam nec odio vestibulum est mattis effic iturut magna.
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="progress blue">
						<span class="progress-left">
							<span class="progress-bar"></span>
						</span>
						<span class="progress-right">
							<span class="progress-bar"></span>
						</span>
						<div class="progress-value">75%</div>
					</div>
					<div class="progress-bit-title">
						<div class="titles">Photos TAken</div>
						<div class="subtitle">Etiam nec odio </div>
					</div>
				</div>
		 
				<div class="col-md-4 col-sm-6">
					<div class="progress blue">
						<span class="progress-left">
							<span class="progress-bar"></span>
						</span>
						<span class="progress-right">
							<span class="progress-bar"></span>
						</span>
						<div class="progress-value">83%</div>
					</div>
					<div class="progress-bit-title">
						<div class="titles">Km Walked</div>
						<div class="subtitle">Etiam nec odio </div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="progress blue">
						<span class="progress-left">
							<span class="progress-bar"></span>
						</span>
						<span class="progress-right">
							<span class="progress-bar"></span>
						</span>
						<div class="progress-value">25%</div>
					</div>
					<div class="progress-bit-title">
						<div class="titles">Cities Vissited</div>
						<div class="subtitle">Etiam nec odio </div>
					</div>
				</div>
			</div>
      </div>
    </section>
	
	<section class="section4">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="viewsec4">
						<div class="box-main-sec4">
							<div class="toplabel">
								<label class="dark-color">BTC/USD</label>
								<span class="pink-color">-1.25%</span>
							</div>						
							<div class="bottomlabel">
								<div class="title">
									<span class="pink-color">$8120.87</span> 
									<span class="gray-color"></span>
								</div>
								<div class="discrip">
									<label class="gray-color">Volume:</label>
									<span class="gray-color">5,765.64 BTC</span>
								</div>
							</div>
						</div>
						<div class="sec4-view1"></div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="viewsec4">
						<div class="box-main-sec4">
							<div class="toplabel">
								<label class="dark-color">BTC/USD</label>
								<span class="pink-color">-1.25%</span>
							</div>						
							<div class="bottomlabel">
								<div class="title">
									<span class="pink-color">$8120.87</span> 
									<span class="gray-color">$750.87</span>
								</div>
								<div class="discrip">
									<label class="gray-color">Volume:</label>
									<span class="gray-color">5,765.64 BTC</span>
								</div>
							</div>
						</div>
						<div class="sec4-view3"></div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="viewsec4">
						<div class="box-main-sec4">
							<div class="toplabel">
								<label class="dark-color">BTC/USD</label>
								<span class="pink-color">-1.25%</span>
							</div>						
							<div class="bottomlabel">
								<div class="title">
									<span class="pink-color">$8120.87</span> 
									<span class="gray-color">$750.87</span>
								</div>
								<div class="discrip">
									<label class="gray-color">Volume:</label>
									<span class="gray-color">5,765.64 BTC</span>
								</div>
							</div>
						</div>
						<div class="sec4-view3"></div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="viewsec4">
						<div class="box-main-sec4">
							<div class="toplabel">
								<label class="dark-color">BTC/USD</label>
								<span class="green-color">-1.25%</span>
							</div>						
							<div class="bottomlabel">
								<div class="title">
									<span class="drak-color">$8120.87</span> 
									<span class="gray-color">$750.87</span>
								</div>
								<div class="discrip">
									<label class="gray-color">Volume:</label>
									<span class="gray-color">5,765.64 BTC</span>
								</div>
							</div>
						</div>
						<div class="sec4-view4"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section5">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!-- Nav tabs -->
					<div class="card">					
						<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
							<li class="active"><a href="#favorites" data-toggle="tab">Favorites</a></li>
							<li><a href="#btcmarkets" data-toggle="tab">BTC Markets</a></li>
							<li><a href="#ethmarkets" data-toggle="tab">ETH Markets</a></li>
							<li><a href="#usdmarkets" data-toggle="tab">USDT Markets</a></li>
							<!--<li><a href="#markets1" data-toggle="tab">Markets 1</a></li>
							<li><a href="#markets2" data-toggle="tab">Markets 2</a></li>
							<li><a href="#markets3" data-toggle="tab">Markets 3</a></li>-->
						</ul>
						<div id="my-tab-content" class="tab-content">
							<div class="tab-pane active" id="favorites">
								<div class="tradingview-widget-container">
								  <div class="tradingview-widget-container__widget"></div>
								  <div class="tradingview-widget-copyright"><a href="https://in.tradingview.com/markets/cryptocurrencies/prices-all/" rel="noopener" target="_blank"><span class="blue-text">Cryptocurrency Markets</span> by TradingView</a></div>
								  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-screener.js" async>
								  </script>
								</div>
							</div>
							<div class="tab-pane" id="btcmarkets">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th></th>
										<th>Pair</th>
										<th>Last Price</th>
										<th>24h Change</th>
										<th>24h High</th>
										<th>24h Low</th>
										<th>24h Volume</th>
									  </tr>
									</thead>
									<tbody>
									 									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									</tbody>
								  </table>
							</div>
							<div class="tab-pane" id="ethmarkets">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th></th>
										<th>Pair</th>
										<th>Last Price</th>
										<th>24h Change</th>
										<th>24h High</th>
										<th>24h Low</th>
										<th>24h Volume</th>
									  </tr>
									</thead>
									<tbody>
									 									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									</tbody>
								  </table>
							</div>
							<div class="tab-pane" id="usdmarkets">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th></th>
										<th>Pair</th>
										<th>Last Price</th>
										<th>24h Change</th>
										<th>24h High</th>
										<th>24h Low</th>
										<th>24h Volume</th>
									  </tr>
									</thead>
									<tbody>
									 									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star active"></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="red-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="red-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									  <tr>
										<td><i class="far fa-star "></i></td>
										<td class="first">TRX/BTC </td>
										<td><span class="green-color">0.00000618</span> / <span>$0.05</span></td>
										<td class="green-color">8.04% </td>
										<td>0.00000633</td>
										<td>0.00000553 </td>
										<td>13,102.00341589</td>
									  </tr>
									</tbody>
								  </table>
							</div>
							
						</div>
						
					</div>					
                </div>
			</div>
		</div>
	</section>
    <!-- Footer -->
    <footer class="dark-footer">
      <div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-3">
				<div class="abloutftr">
					<a href="#"><img src="{{asset('/public/gilt/img/footerlogo.png')}}"/></a>
					<p>
						Etiam nec odio vestibulum est mattis effic iturut magna. Pellent esque sit amet tellus blandit. Etiam nec odio vestibul.
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				<div class="footer-title">Fast links</div>
				<div class="spl-link">
					<ul>
						<li><a href="#" target="_blank">Home</a></li>
						<li><a href="#" target="_blank" class="">Testimonials</a></li>
						<li><a href="#" target="_blank">Services & Features</a></li>
						<li><a href="#" target="_blank">Accordions and tabs</a></li>
						<li><a href="#" target="_blank">Menu ideas</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				<div class="footer-title">Fast links</div>
				<div class="blog-footer">
					<div class="view-blogs">
						<div class="date">23 September, 2017</div>
						<div class="titles"><a href="#">Why we love stock photos</a></div>
						<div class="author">By  Mark Johnson</div>
					</div>
					<div class="view-blogs">
						<div class="date">23 September, 2017</div>
						<div class="titles"><a href="#">Why we love stock photos</a></div>
						<div class="author">By  Mark Johnson</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				<div class="footer-title">Contact Info</div>
				<div class="contact-main">
					<div class="view-cont">
						<span class="left">
							<i class="fa fa-map-marker"></i> 
						</span>
						<span class="right">419-A, Wave Silver Tower, Sector-18, Noida - 201301</span>
					</div>
					<div class="view-cont">
						<span class="left">
							<i class="fa fa-phone-volume"></i>
						</span>
						<span class="right">0120 - 4265782</span>
					</div>
					<div class="view-cont">
						<span class="left">
							<i class="far fa-envelope-open"></i> 
						</span>
						<span class="right"><a href="mailto:support@giltxchange.com">support@giltxchange.com</a></span>
					</div>
				</div>
			</div>
		</div>       
      </div>
	</footer>
@endsection()
@section('footerjs')
	<script src="{{asset('/public/js/secure.js')}}"></script>
@endsection()
