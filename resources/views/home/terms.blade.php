@extends('home.master')
@section('headerjs')
@endsection()

@section('content')
<!--===================== End of Header ========================-->
	<!--===================== First Section ========================-->
	<div class="first-section animatedParent" style="padding: 75px 0 50px 0; margin-bottom: 50px;" >
		<div class="container">
			
			<div class="row">
				<div class="col-xl-12">
					<div class="text text-center" style="max-width:100%" >
						<h1>Terms & Conditions</h1>
					</div>
				</div>
			</div>
			
			<!--<div class="row" style="display:none;" >
				<div class="col-xl-6 col-lg-6 col-md-6 col-12 animated bounceInLeft">
					<div class="text">
						<h1>Terms & Condition</h1>
						<span class="d-flex align-items-center"><b class="line">&nbsp;</b>More like a family<b class="line">&nbsp;</b></span>
					</div>
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6 col-12 animated bounceInRight position-relative">
					<img src="wp-content/themes/cryptocurrency-html/assets/img/about-img.jpg" alt="about-img">					
				</div>
			</div>-->			
			
		</div>
		<div class="cloud">&nbsp;</div>
		<div class="cloud-two">&nbsp;</div>
		<div class="mini-cloud"></div>
		<div class="mini-cloud two"></div>
		<div class="mini-cloud three"></div>
	</div>
	<!--===================== End of First Section ========================-->
	<div class="container">
		<div class="row">
			<div class="col-xl-12 text-about">
				<!--<h2 class="h2-main">Fees</h2>-->
				<p><strong>ACCEPTANCE</strong></p>				
				<p>Giltxchange Global Private Limited ("Giltxchange") reserves the right to update and change, from time to time, these Terms and Conditions, the Privacy Policy, the KYC and Anti Fraud Policy and all other documents incorporated herein by reference without any notice to or re-acceptance from the User. By registering with Giltxchange and creating your account ("Account") to use the live trade engine for trading in Virtual Currencies ("Trade Engine"), you ("User") are hereby agreeing to be bound by these terms and conditions ("Terms and Conditions").</p>
				
				<p>&nbsp;</p>
				
				<p><strong>DISCLAIMER</strong></p>								
				<p>Giltxchange only provides a platform for trading in virtual currencies and crypto assets (hereby referred as 'Virtual Currencies'). The User hereby expressly agrees and warrants that: The User is aware that Virtual Currencies are volatile commodity whose price is subject to sharp changes which may cause large profits or losses to the user; Giltxchange has in no way induced or lured the User into opening an Account with Giltxchange to trade in Virtual Currencies; The money used to buy the Virtual Currencies has been received by the User through legal means and channels is not in any way connected with any illegal or fraudulent activity; The User is the legitimate owner and is allowed to use all Virtual Currencies deposited on his/her Account and that the transactions being carried out by the User do not infringe the rights of any third party or any applicable laws; All information provided by the User to Giltxchange including all identity, personal and bank information as well as documents are true and accurate in all respects; The Users shall use the Account with Giltxchange only for non commercial activities; The User is above 18 years of age and has the legal and mental capacity to enter into legally binding contracts with third parties; and The User is aware that Giltxchange may change these Terms and Conditions at any time. </p>
				
				<p>&nbsp;</p>
				
				<p>Giltxchange and its owners/affiliates are not liable for any damages caused by any performance, failure of performance, error, omission, interruption, deletion, defect, delay in transmission or operations, computer virus, communications line failure, and unauthorized access to the personal accounts. Giltxchange is not responsible for any technical failure or malfunction of the software or delays of any kind. We are also not responsible for non-receipt of registration details or e-mails. Users shall bear all responsibility of keeping the password secure. Giltxchange is not responsible for the loss or misuse of the password. 
				</p>
				
				<p>&nbsp;</p>
				
				<p>Giltxchange is not responsible for the content of any of the linked sites. By providing access to other web-sites, Giltxchange is neither recommending nor endorsing the content available in the linked websites. 
				</p>
				
				<p>&nbsp;</p>
				
				<p>Giltxchange and the User are aware that although there is no legal bar or impediment to trading in Virtual Currencies in India at the moment, the Reserve Bank of India (RBI) has issued a press release no. 2013-2014/1261 dated 24th December 2013 whereby it has stated that "The creation, trading or usage of VCs including Bitcoins, as a medium for payment are not authorised by any central bank or monetary authority. No regulatory approvals, registration or authorisation is stated to have been obtained by the entities concerned for carrying on such activities. As such, they may pose several risks to their users". The press release further states that the RBI is still "examining the issues associated with the usage, holding and trading of VCs under the extant legal and regulatory framework of the country". The full text of the said press release can be found at Here The User is hereby made aware of the nebulous legal situation regarding Virtual Currencies and Giltxchange shall not be liable for any proceedings against the User in any Court of law or before any governmental, administrative, regulatory, judicial or quasi judicial authority. 
				</p>
				
				<p>&nbsp;</p>
				
				<p>Although the press release by the RBI mentions the risks associated with all virtual currencies including Virtual Currencies, it does not specifically bar them. Keeping in mind some of the risks mentioned in the press release by the RBI Giltxchange has drafted a Privacy Policy as well as a KYC Policy which are available on the Giltxchange website and the User specifically agrees to the terms contained in both the above mentioned policies.
				</p>
				
				<p>&nbsp;</p>								
				<p><strong>RISKS</strong></p>
								
				<p>The User acknowledges that he is fully aware of the risks involved in online trading activities, including the risk involved due to unauthorized access or any technical difficulties. Furthermore, in a technical environment, should an error occur with respect to the tracking of any account holding or order entry, the true, actual and correct transaction or position may not be restored. It is the User's responsibility to ensure account correctness and accuracy and to contact Giltxchange immediately in respect of any discrepancies. 	
				</p>
				
				<p>&nbsp;</p>
				
				<p>Users are advised to use the content and data on the website for the purpose of information only and rely on their own judgment while making investment or trading decisions. In case Giltxchange gives any information on the website, such information shall not be considered as advice of any kind and the User shall use its own judgment to make all decisions. 	
				</p>
				
				<p>&nbsp;</p>
				
				<p>Giltxchange does not make any personal recommendations. The information on our website is provided solely to enable Users to make their own investment or trading decisions and does not constitute a recommendation to buy, sell or otherwise deal in Virtual Currencies. 	
				</p>
				
				<p>&nbsp;</p>
				
				<p>The price and value of investments in Virtual Currencies and the income derived from them can go up or down and you may not get back the capital invested. Changes in the rate of exchange may have an adverse effect on the value, price and value of your investment. Past performance is not necessarily an indicator of future performance. The services and investments in Virtual Currencies may have tax consequences and it is important to bear in mind that Giltxchange does not provide any tax advice regarding Virtual Currencies and the increase or decrease in their value. The level of taxation depends on individual circumstances and such levels and bases of taxation can change. You should consult your own Tax Advisor in order to understand any applicable tax consequences. 	
				</p>
				
				<p>&nbsp;</p>
				
				<p>Virtual Currencies trading has specific risks, which are not shared with other official currencies, goods or commodities in a market. Unlike most currencies, which are supported by government reserves or other legal entities, as well as commodities such as silver and gold, Virtual Currencies is a "flat" currency, which is only backed by mathematics, technology and trust. The currency is absolutely decentralized, which means there is no authority that can take corrective measure to protect the Virtual Currencies value in a crisis or issue more currency. When using Virtual Currencies - traders put their trust in the digital, decentralized and mostly anonymous system, which relies on P2P networking and cryptography to maintain its integrity. Trading in Virtual Currencies is susceptible to irrational or rational bubbles or absolute loss of confidence, which could collapse demand/supply. Any actions, even remotely connected to Virtual Currencies can crash confidence in this currency, such as unexpected changes imposed by the currency developers, a government crackdown, the creation of a superior competing Virtual Currencies alternative, or even a deflation or inflation spiral. Confidence might also collapse because of various technical problems: if the anonymity of the system can be compromised, funds lost or stolen, or in the event that hackers or governments become able to prevent Virtual Currencies transactions from settling. Giltxchange shall not be liable for any loss caused to the User due to any of the risks related to trading in Virtual Currencies, whether currently known or unknown. 	
				</p>
				
				<p>&nbsp;</p>
				
				<p>Virtual Currencies purchased using a bank account or credit card or any other deposit method may be reversed at a later time, for example, if such a payment is subject to a chargeback, reversal, claim or is otherwise invalidated. A Virtual Currencies transaction may be unconfirmed for a period of time (usually less than one hour, but up to one business day) and never complete if it is in a pending state. 	
				</p>
				
				<p>&nbsp;</p>
				
				<p>The content of the website cannot be copied, reproduced, republished, uploaded, posted, transmitted or distributed for any non-personal use without obtaining prior permission from Giltxchange. We reserve the right to terminate the accounts of Users who violate the proprietary rights, in addition to necessary legal action. 	
				</p>
				
				<p>&nbsp;</p>
				
				<p>Users agree that the information gathered from the Users' profiles may be used to enhance their experience on the website. Giltxchange will not rent or sell the profile to any third party other than in accordance with the Privacy Policy. In case of a contest or a promotion scheme, Giltxchange reserves the right to share the User profiles with the sponsors. In the event of necessary credit checks or collection of payments or taxes, Giltxchange can disclose such information to other authorities including income tax department in good faith. 	
				</p>
				
				
				<p>&nbsp;</p>
				
				<p>Price and availability of products and services offered on the site are subject to change without prior notice. Giltxchange may provide information about the availability of products or services to a certain extent but the Users should not rely on such information. Giltxchange will not be liable for any lack of availability of products and services the User may order through the site. 	
				</p>
				
				<p>&nbsp;</p>
				
				<p>Users agree that they understand the meaning of Limit Order and Market Order and the risk involved in placing orders in the order book. Giltxchange shall not be liable for any loss caused to the User due to any of the risks involved while placing orders.	
				</p>
				
				
				<p>&nbsp;</p>				
				<p><strong>REGISTRATION</strong></p>
								
				<p>General: The User hereby agrees and warrants that all information and particulars provided by the User has been provided voluntarily without any inducement or pressure and is true to the best of his/her knowledge and belief. In case of any particulars provided by the User being incorrect or fraudulent, Giltxchange shall be at liberty to terminate the User's account. Giltxchange further reserves the right, at its sole discretion, to deny the User access to the Trade Engine or any portion thereof without notice for the following reasons: (a) any unauthorized or illegal access or use by the User; (b) any assignment or transfer (or attempt to do the same) of any rights granted to the User under these Terms and Conditions; (c) any violation of these Terms and Conditions.  	
				</p>
				
				<p>&nbsp;</p>
				
				<p>Using Google ID: Users may be given the option of signing in using their Google IDs. Logging on to Giltxchange with the Gmail account gives the User the convenience of not having to remember yet another password - without compromising on privacy, or the safety of the individual email account. Giltxchange does not store the User's Google password information. The password of every User who chooses to log in using the Google ID is sent to Google for verification, and is not saved on our systems.   	
				</p>
				
				<p>&nbsp;</p>
				
				<p>Single Registration: Each individual is allowed to register only once and no person is allowed to create multiple Accounts. If any User is found to be using or registered with multiple Accounts, such User's access to the Trade Engine may be terminated. The use of scripts or any automated system to register is strictly prohibited. Giltxchange assumes no responsibility for lost, late, incomplete, incorrect, or misdirected registrations. Giltxchange is not responsible for technical, hardware or software malfunctions, misdirected entries, lost or unavailable network connections, or other communications or other technical problems related to the registration.  	
				</p>
				
				<p>&nbsp;</p>
				
				<p>Change of e-mail ID is not permitted. Giltxchange is not responsible for any loss to the users if the users account is compromised.  
				</p>
				
				<p>&nbsp;</p>
				
				
				<p>Deposits and Withdrawals of INR: All the withdrawal requests for INR may take upto 24 hours to process. Only "authorized" withdrawal requests will be considered for withdrawal. Giltxchange can not guarantee a withdrawal in the said time frame and may take upto a week based on the bank's discretion. Deposits of INR may be reflected in the user's wallet within 24 hours from the time Giltxchange receives the funds in the mentioned bank account. Giltxchange does not guarantee for the user's balance to get updated in the said time frame.  	
				</p>
				
				<p>&nbsp;</p>				
				<p><strong>LINKING OF BANK ACCOUNT</strong></p>	
								
				<p>Giltxchange shall accept deposits from an account held by the User in any scheduled Indian bank. The process of linking shall be as follows: Click on the "My Profile" link in the dropdown menu under "Account" on the web page after logging into your Account. Clock the "Bank" link on the "My Profile" page. Fill in the bank details requested on the web page including bank account number, IFSC Code, etc. and submit the same. Your Giltxchange Account will, after verification, automatically get linked to your Bank Account.   	
				</p>			
				
				<p>&nbsp;</p>			
				<p>Giltxchange shall accept deposits from the verified bank accounts only. Any deposit made from unverified bank account may not be credited to user's wallet. For change of bank account, an user must send an e-mail to Giltxchange stating the reason and attaching relevant bank documents.   	
				</p>
				
				<p>&nbsp;</p>
				<p>Giltxchange expressly disclaims all responsibility for any loss caused to the User due to the linking the User's Giltxchange Account with the User's bank account or while executing any trades using Giltxchange, for whatever reason, unless the same has been caused due to the gross negligence of Giltxchange.   	
				</p>
				
				
				<p>&nbsp;</p>
				<p>Directing the proceeds of a sale to a third party is expressly prohibited and will result in immediate suspension of the Account. A third party is any party other than the User and Giltxchange. The User is expressly prohibited from opening an account under a third party or fictitious name for the purpose of using this service to pay a third party. Users are expressly prohibited from registering a payment method for a third party in their Giltxchange Account for the purpose of using this service to pay a third party. Third party transactions are expressly prohibited and will result in immediate suspension and termination of the Account. 
				</p>
				
				
				<p>&nbsp;</p>
				<p><strong>RULES</strong></p>								
				<p>"The Trade Engine" is a live Virtual Currencies trading platform whereby the Users can use real money to buy and sell actual Virtual Currencies from their Accounts with Giltxchange.  	
				</p>
				
				<p>&nbsp;</p>
				
				<p><strong>Opening Account And Starting – </strong></p>
				<p>Once the User creates an Account by registering with Giltxchange, the User will be allowed to place orders in the exchange area. Each Account will be linked with the User's bank account as per the details provided by the User who can use the money in such linked bank account and start trading with other users. Once the User enters the trading area the User will see the following displays: </p>
				
				
				<p><strong>My Wallet – </strong></p>
				<p>The Users will also be able to view the available and on hold balance in their account at all times. A Users account balance shall display the Virtual Currencies balance as well as INR balance currently in the User's account. </p>
				
				<p><strong>Markets –</strong></p>
				<p>The Users will also be able to view the available markets where he/she can trade at any time. The market will display the "Last Price", "Best Bid" and "Best Ask" as per Giltxchange. We do not show any other exchanges' prices. </p>
				
				
				<p><strong>Orders – </strong></p>
				<p>"Orders" is the trade book where an user can see the available orders from all other users to be fulfilled. "Orders" shows "Bid" and "Ask". A "Bid" table contains the details of the orders which shall be filled by placing a "Sell order". An "Ask" table contains the details of the orders which shall be filled by placing a "Buy order". When an order is placed, the trade engine automatically matches it with other orders in the trade book at best prices and available volumes. </p>
				
				
				<p><strong>My Orders –</strong></p>
				<p>"My Orders" displays all of the User's pending trade requests (Both Buy and Sell transactions) that are yet to be executed/ completed /cancelled/ partially filled. In the event that a User's trade has not been completed/ partially filled, the User can choose to cancel the order by clicking on the 'X' icon corresponding to such trade request that is pending. Please note that a trade request can only be cancelled when it's still pending and reflecting under the 'Open' link in 'My Orders'. Once a transaction has been executed/ completed, it may no longer be cancelled. Under the 'Closed' link in 'My Orders', users will be able to see all the completed/ cancelled trades (both Buy and Sell transactions) pertaining to the User. </p>
				
				
				<p>&nbsp;</p>
				
				<p><strong>Buying Virtual Currencies</strong> </p>
				<p><strong>Step 1:</strong> In the event that User intends to purchase Virtual Currencies, the User is required to place a trade request with the exchange platform for the same. User can choose to place orders at Limit or at Market. In order to place a limit order, user shall specify the amount of Virtual Currencies and the price at which user intends to buy. In order to place a market order, user shall specify how much he/she is willing to pay in total for the order. User shall understand the risk of any losses occured due to lack of knowledge of the "Limit Order" or "Market Order". User can place order in the order book by clicking the button "Buy"</p>
								
				<p><strong>Step 2:</strong>Once the User clicks on "Buy", the trade engine automatically picks the order and executes it with other orders in the order book if orders match or keep it in the order book for future executions</p>
								
				<p><strong>Step 3:</strong>In the event that there is a pending "Sell Order" for the price at which the User's bid has been placed, the order would be executed immediately and the User's balance would reflect the updated statistics. This can be achieved by matching the User's order with a single order of such quantity and price, if available or if a single order with the required price and amount is not pending, the trade engine will complete the User's order by matching it with multiple orders and then complete the User's order in multiple tranches. </p>
								
				<p><strong>Step 4:</strong>If a User's trade request does not match with any of the existing requests on the trading platform, then such request shall automatically move into the waiting list of pending transactions for it to be matched with an appropriate "Sell Order" as and when such a request is received. In this case, the User's balance would remain intact until the transaction is completed. While the transaction is pending, the User may cancel the transaction through the "My Orders" section as explained above.</p>
				<p>&nbsp;</p>
				
				
				<p><strong>Selling Virtual Currencies</strong></p>				
				<p><strong>Step 1:</strong> In the event that User intends to sell Virtual Currencies, the User is required to place a trade request with the exchange platform for the same. User can choose to place orders at Limit or at Market. In order to place a limit order, user shall specify the amount of Virtual Currencies and the price at which user intends to sell. In order to place a market order, user shall specify how much volume of Virtual Currencies, he/she is willing to sell in total for the order. User shall understand the risk of any losses occured due to lack of knowledge of the "Limit Order" or "Market Order". User can place order in the order book by clicking the button "Sell". </p>
				
				<p><strong>Step 2:</strong> Once the User clicks on "Sell", the trade engine automatically picks the order and executes it with other orders in the order book if orders match or keep it in the order book for future executions.  </p>
				
				<p><strong>Step 3:</strong> In the event that there is a pending "Buy Order" for the price at which the User's bid has been placed, the order would be executed immediately and the User's balance would reflect the updated statistics. This can be achieved by matching the User's order with a single order of such quantity and price, if available or if a single order with the required price and amount is not pending, the trade engine will complete the User's order by matching it with multiple orders and then complete the User's order in multiple tranches.</p>
				
				<p><strong>Step 4:</strong> If a User's trade request does not match with any of the existing requests on the trading platform, then such request shall automatically move into the waiting list of pending transactions for it to be matched with an appropriate "Buy Order" as and when such a request is received. In this case, the User's balance would remain intact until the transaction is completed. While the transaction is pending, the User may cancel the transaction through the "My Orders" section as explained above.</p>
				
				<p>&nbsp;</p>
				<p><strong>FEES</strong></p>
				<p>The participation by the User in the exchange platform does not in any way constitute any gambling, betting, wager, speculation, lottery, raffle, sweepstakes, etc. in any form whatsoever. There has been no collection of any money, cash or other valuable thing from the User by Giltxchange for any such purpose. Giltxchange only charges a fee from its Users for the services of providing the Trade Engine. The Fee Schedule is subject to change from time to time at the sole discretion of Giltxchange.</p>
				<p>&nbsp;</p>
				
				<p><strong>GENERAL INDEMNITY AND WAIVER</strong></p>
				<p>By registering and creating an account the Users assume sole liability for and agree to indemnify and hold Giltxchange, its affiliates, officers, directors, employees and agents, harmless from and against any and all claims, damages, obligations, losses, injuries, costs or debt, and expenses (including but not limited to attorney's fees) claimed to be caused from usage of the Trade Engine or the acceptance, possession and handling, loss, use or misuse of any money or Virtual Currencies in their Account. Giltxchange reserves the right to cancel, suspend the exchange platform, or any part of it, if any fraud, technical failures, computer virus, bug, tampering, or any other factor impairs the integrity or proper functioning of the promotion, as determined by Giltxchange in its sole discretion. By participating registering and creating an account, the Users agree to be bound by these Terms and Conditions and the decisions of Giltxchange and to be contacted by Giltxchange or its agents by telephone, sms, mail and/or email regarding the Trade Engine or any other products that Giltxchange may offer now or in future. </p>
				<p>&nbsp;</p>
				<p>The User specifically agrees that Giltxchange shall not be liable for any loss, actual or perceived, caused directly or indirectly by government restriction, exchange or market regulation, suspension of trading, war, strike, equipment failure, communication line failure, system failure, security failure on the Internet, unauthorised access, theft, or any problem, technological or otherwise, or other condition beyond the control of Giltxchange. The User further agrees that it will not be compensated by Giltxchange for "lost opportunity" viz., notional profits on buy/sell orders which could not be executed.</p>
				<p>&nbsp;</p>
				
				<p><strong>ELIGIBILITY</strong></p>
				<p>The registrations are open to persons from countries where virtual currencies are not illegal, are 18 years of age or older at the time of registration and are of sound mind and able to enter into legally enforceable contracts. Users residing within the territorial borders of India shall only be allowed to make deposit and withdrawal requests of INR.</p>
				<p>&nbsp;</p>
				
				
				<p><strong>NO INDUCEMENT OF THE USER</strong></p>
				<p>The User hereby expressly acknowledges and agrees that the User has initiated the act of registering on Giltxchange entirely of his/her own free will and Giltxchange has not caused, induced, urged, prompted, instigated, drawn, compelled, persuaded or engendered the User to open an Account and trade in Virtual Currencies in any manner or form.</p>
				
				<p>&nbsp;</p>
				<p><strong>DOWNTIME</strong></p>
				<p>The User acknowledges and agrees that Giltxchange may from time to time need to make structural changes to the trading platform that could result in system downtime, i.e. the website and the trading platform being unavailable for access. Any such changes will (where possible or necessary) be broadcast in advance to all Users, and carried out at times which Giltxchange considers will cause the least disruption.</p>
				
				<p>&nbsp;</p>				
				<p>Giltxchange shall have no liability to the Users for any downtime to the trading platform for any reason whatsoever, nor for any downtime caused by general Internet failure, the failure of any Internet connection or the failure of any User equipment.</p>
				<p>&nbsp;</p>
				
				
				<p><strong>GRIEVANCE REDRESSAL</strong></p>
				<p>In case of any grievance or complaints with the website or its performance please contact Giltxchange at support[at]Giltxchange[dot]com. Once a complaint has been received, all endeavors will be made to address the complaint as soon as possible.</p>
				<p>&nbsp;</p>
				
				
				<p><strong>GENERAL CONDITIONS</strong></p>
				<p>Giltxchange may prohibit any User, if Giltxchange is of the opinion that the User is showing a disregard for these Terms and Conditions, or acts (1) in an unfair or fraudulent manner; (2) with an intent to annoy, abuse, threaten, or harass any other User or Giltxchange; or (3) in any other disruptive manner.</p>
				<p>&nbsp;</p>
				<p>The use of software, machines, computers, scripts or any automated system is strictly prohibited. If any User tries to influence the rate of exchange of Virtual Currencies or gain an unfair advantage in any manner whatsoever, such User shall be liable to be prohibited from conducting any further transactions on the Trade Engine and may be reported to the appropriate authorities. Any decision in this regard shall be taken at the sole discretion of Giltxchange and shall be final. 
				</p>
				<p>&nbsp;</p>
				<p>Any attempt by a User or any other individual to deliberately damage the website or undermine the legitimate operation of Giltxchange is a violation of criminal and civil laws and should such an attempt be made, Giltxchange reserves the right to seek damages from any such User to the fullest extent permitted by law.  
				</p>
				<p>&nbsp;</p>
				<p>Completion of a transaction may be stopped by Giltxchange at any time if Giltxchange, in good faith, believes that such transaction may be fraudulent or may adversely impact the Virtual Currencies economy, or may adversely impact the exchange rate of Virtual Currencies, etc. 
				</p>
				<p>&nbsp;</p>
				<p>The Giltxchange website is for the exclusive purpose of transactions to be carried out within the territorial jurisdiction of India and all such transactions shall be governed by the laws in India. Notice is hereby given that Non Resident Indians (NRI's) and Foreign Nationals accessing this web site and opting to transact thereon shall do so after due verification at their end of their eligibility to do so. Giltxchange undertakes no responsibility for such pre-eligibility of qualification on part of Non-Resident Indians (NRI's) or Foreign Nationals to transact on this website.  
				</p>
				<p>&nbsp;</p>
				<p>The User agrees that, whenever a transaction is made, the Trade Engine sends and receives the monetary sums and/or Virtual Currencies from the User's Account in the User's name and on the User's behalf, through the system.  
				</p>
				<p>&nbsp;</p>
				<p>The User is responsible for maintaining the confidentiality of information on his/her Account, including, but not limited to the password, email, wallet address, wallet balance, and of all activity including transactions made through the Account. If there is suspicious activity related to the User's Account, Giltxchange may request additional information from the User, including authenticating documents, and freeze the Account for the review time. The User is obligated to comply with these security requests, or accept termination of the Account.  
				</p>
				<p>&nbsp;</p>
				<p>The User shall be solely responsible for ensuring that any transfer of Virtual Currencies to a transferee shall be a valid and legal transaction not infringing any laws including money-laundering laws and regulations of India or any other applicable jurisdiction. 
				</p>
				<p>&nbsp;</p>
				<p>Giltxchange reserves the right to cancel unconfirmed Accounts or Accounts that have been inactive for a period of six (6) months or more, and/or to modify or discontinue exchange platform or the Trade Engine. The User agrees that Giltxchange will not be liable to them or to any third party for termination of their Account or access to the website. The suspension of an Account shall not affect the payment of the commissions due for past transactions. Upon termination, Users shall communicate a valid bank account to allow for the transfer of any currencies credited to their account. Virtual Currencies may be transferred to a valid bank account only after conversion into a currency. It can be transferred through Virtual Currencies too at the sole discretion of Giltxchange, if it feels such a transfer is more convenient.
				</p>
				
				<p>&nbsp;</p>
				
				<p><strong>NO LIABILITY</strong></p>
				<p>Giltxchange is not responsible for any incorrect or inaccurate registrations, whether caused by the User or by any of the equipment or programming associated with or utilized in the Trade Engine or by any technical or human error which may occur in the operation of the Trade Engine. The User expressly understands and agrees that under no circumstances shall Giltxchange be liable to any User on account of that User's use or misuse of and reliance on the Giltxchange website. Such limitation of liability shall apply to prevent recovery of direct, indirect, incidental, consequential, special, exemplary, and punitive damages (even if Giltxchange has been advised of the possibility of such damages), whether the damages arises from use or misuse of or reliance on the Trade Engine, from inability to use the Trade Engine, or from the interruption, suspension, of the Trade Engine (including such damages incurred by third parties), or from a sudden loss in the price of Virtual Currencies, or any technical failure or hacking attacks on the Giltxchange Servers, or any change in law or regulatory framework in India. 	
				</p>
				<p>&nbsp;</p>
				<p>All information is provided on an "as is" basis, with no warranties whatsoever. A possibility exists that the site could include inaccuracies or errors. Additionally, a possibility exist that unauthorized additions, deletions or alterations could be made by third parties to the site. Although Giltxchange attempts to ensure the integrity, correctness and authenticity of the site, it makes no guarantees whatsoever as to its completeness, correctness or accuracy. In the event, that such an inaccuracy arises, please inform Giltxchange so that it can be corrected. 	
				</p>
				<p>&nbsp;</p>
				<p>All express, implied, and statutory warranties, including, without limitation, the warranties of merchantability, fitness for a particular purpose, and non-infringement of proprietary rights, are expressly disclaimed to the fullest extent permitted by law. To the fullest extent permitted by law, Giltxchange disclaims any warranties for (i) the security, reliability, timeliness, and performance of the Trade Engine; (ii) other services or goods, as well as for any information or advice received through Giltxchange or through any links provided herein. 	
				</p>
				<p>&nbsp;</p>
				<p>The User agrees that Giltxchange shall not be liable for:</p>
				<p class="ml-3" >- the deletion, failure to store, misdelivery, or untimely delivery of any information or material;</p>
				<p class="ml-3" >- any harm resulting from downloading or accessing any information or material through Giltxchange; </p>
				<p class="ml-3" >- financial loss due to wallet data being "Bruteforced"; </p>
				<p class="ml-3" >- financial loss due to server failure or data loss; </p>
				<p class="ml-3" >- financial loss due to forgotten passwords; </p>
				<p class="ml-3" >- financial loss due to corrupted wallet files; </p>
				<p class="ml-3" >- financial loss due to incorrectly constructed transactions or mistyped Virtual Currencies , Litecoin or other cryprocurrencies addresses; </p>
				<p class="ml-3" >- financial loss due to "phishing" or other websites masquerading as Giltxchange </p>
				
				<p>&nbsp;</p>
				<p>Giltxchange cannot and does not guarantee or warrant that the files available for downloading through the Trade Engine or the Trade Engine itself will be free from infection by software viruses or other harmful computer code, files or programs. Giltxchange shall not be responsible for any software viruses or other harmful computer code, files or programs that the user may encounter due to your visiting this trade engine. 	
				</p>
				<p>&nbsp;</p>
				<p>Giltxchange has not reviewed all of the sites linked to its trade engine and is not responsible for the contents of any such linked sites. The inclusion of any link does not imply Giltxchange's endorsement of the site. Use of any such linked website(s) is at your own risk	
				</p>
				<p>&nbsp;</p>
				<p>If the User has a dispute with one or more users, the User hereby releases Giltxchange (and our parent, officers, directors, agents, joint ventures, employees and suppliers) from any and all claims, demands and damages (actual and consequential) of every kind and nature arising out of or in any way connected with such disputes. In addition, the User waives any protection available to such User under any law in the User's jurisdiction, which says that a general release does not extend to claims which the creditor does not know or suspect to exist in his favor at the time of executing the release, which if not known by him must have materially affected his settlement with the debtor.	
				</p>
				<p>&nbsp;</p>
				
				
				<p><strong>GENERAL PROHIBITIONS</strong></p>
				<p>The Users agree not to do any of the following:</p>
				<p class="ml-3 mt-3" >-	access the website via anonymous networks for view and information purpose; </p>
				<p class="ml-3" >-	use anonymous service, networks such as TOR to do business with Giltxchange. Any orders placed to our website via anonymous networks such as TOR etc will be canceled and will result in Order Cancellation fees; </p>
				<p class="ml-3" >-	open multiple Accounts with Giltxchange;</p>
				<p class="ml-3" >-	defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others;</p>
				<p class="ml-3" >-	publish, post, distribute or disseminate any defamatory, infringing, obscene, indecent or unlawful material or information; </p>
				<p class="ml-3" >-	upload files that contain software or other material protected by intellectual property laws (or by rights of privacy of publicity) unless you own or control the rights thereto or have received all necessary consents;</p>
				<p class="ml-3" >-	upload files that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of another's computer;</p>
				<p class="ml-3" >-	take any action that imposes an unreasonable or disproportionately large load on Giltxchange's infrastructure; or detrimentally interfere with, intercept, or expropriate any system, data, or information;</p>
				<p class="ml-3" >-	use any Account other than his/her own, or access the Account of any other User at any time, or assist others in obtaining unauthorized access;</p>
				<p class="ml-3" >-	conduct or forward surveys, contests, or chain letters; or download any file posted by another user of a forum that you know, or reasonably should know, cannot be legally distributed in such manner.</p>
				
				<p>&nbsp;</p>
				<p>The usage of the Giltxchange platform by the Users shall not relate to sales of:</p>
				
				<p class="ml-3 mt-3" >-	 narcotics, research chemicals or any controlled substances;</p>
				<p class="ml-3" >-	Adult goods and services which includes pornography and other sexually suggestive materials (including literature, imagery and other media); escort or prostitution services; </p>
				<p class="ml-3" >-	Alcohol which includes Alcohol or alcoholic beverages such as beer, liquor, wine, or champagne;</p>
				<p class="ml-3" >-	Body parts which includes organs or other body parts;</p>
				<p class="ml-3" >-	Bulk marketing tools which includes email lists, software, or other products enabling unsolicited email messages (spam);</p>
				<p class="ml-3" >-	Cable descramblers and black boxes which includes devices intended to obtain cable and satellite signals for free;</p>
				<p class="ml-3" >-	Child pornography which includes pornographic materials involving minors;</p>
				<p class="ml-3" >-	Copyright unlocking devices which includes Mod chips or other devices designed to circumvent copyright protection;</p>
				<p class="ml-3" >-	Copyrighted media which includes unauthorized copies of books, music, movies, and other licensed or protected materials;</p>
				<p class="ml-3" >-	Copyrighted software which includes unauthorized copies of software, video games and other licensed or protected materials, including OEM or bundled software;</p>
				<p class="ml-3" >-	Counterfeit and unauthorized goods which includes replicas or imitations of designer goods; items without a celebrity endorsement that would normally require such an association; fake autographs, counterfeit stamps, and other potentially unauthorized goods;</p>
				<p class="ml-3" >-	Drugs and drug paraphernalia which includes illegal drugs and drug accessories, including herbal drugs like salvia and magic mushrooms;</p>
				<p class="ml-3" >-	Drug test circumvention aids which includes drug cleansing shakes, urine test additives, and related items;</p>
				<p class="ml-3" >-	Endangered species which includes plants, animals or other organisms (including product derivatives) in danger of extinction;</p>
				<p class="ml-3" >-	Gaming/gambling which includes lottery tickets, sports bets, memberships/ enrollment in online gambling sites, and related content;</p>
				<p class="ml-3" >-	Government IDs or documents which includes fake IDs, passports, diplomas, and noble titles;</p>
				<p class="ml-3" >-	Hacking and cracking materials which includes manuals, how-to guides, information, or equipment enabling illegal access to software, servers, websites, or other protected property;</p>
				<p class="ml-3" >-	Illegal goods which includes materials, products, or information promoting illegal goods or enabling illegal acts;</p>
				<p class="ml-3" >-	Miracle cures which includes unsubstantiated cures, remedies or other items marketed as quick health fixes;</p>
				<p class="ml-3" >-	Offensive goods which includes literature, products or other materials that: a) Defame or slander any person or groups of people based on race, ethnicity, national origin, religion, sex, or other factors b) Encourage or incite violent acts c) Promote intolerance or hatred;</p>
				
				<p class="ml-3" >-	Offensive goods, crime which includes crime scene photos or items, such as personal belongings, associated with criminals;</p>
				<p class="ml-3" >-	Prescription drugs or herbal drugs or any kind of online pharmacies which includes drugs or other products requiring a prescription by a licensed medical practitioner;</p>
				<p class="ml-3" >-	Pyrotechnic devices and hazardous materials which includes fireworks and related goods; toxic, flammable, and radioactive materials and substances;</p>
				<p class="ml-3" >-	Regulated goods which includes air bags; batteries containing mercury; Freon or similar substances/refrigerants; chemical/industrial solvents; government uniforms; car titles; license plates; police badges and law enforcement equipment; lock-picking devices; pesticides; postage meters; recalled items; slot machines; surveillance equipment; goods regulated by government or other agency specifications;</p>
				
				
				<p class="ml-3" >- Securities which includes stocks, bonds, or related financial products;</p>
				<p class="ml-3" >- Tobacco and cigarettes which includes cigarettes, cigars, chewing tobacco, and related products;</p>
				<p class="ml-3" >- Traffic devices which includes radar detectors/jammers, license plate covers, traffic signal changers, and related products;</p>
				<p class="ml-3" >- Weapons which includes firearms, ammunition, knives, brass knuckles, gun parts, and other armaments;</p>
				<p class="ml-3" >- Wholesale currency which includes discounted currencies or currency exchanges;</p>
				<p class="ml-3" >- Live animals;</p>
				
				<p class="ml-3" >- Multi-Level Marketing collection fees;</p>
				<p class="ml-3" >- Matrix sites or sites using a matrix scheme approach;</p>
				<p class="ml-3" >- Work-at-home information;</p>
				<p class="ml-3" >- Any product or service which is not in compliance with all applicable laws and regulations whether federal, state, local or international including the laws of India; or any services which compete with Giltxchange.</p>
				
				
				<p>&nbsp;</p>
				<p>The user’s usage of the Giltxchange platform shall not relate to transactions that:</p>
				<p class="ml-3 mt-3" >-	show the personal information of third parties in violation of applicable law;</p>
				<p class="ml-3" >-	support pyramid, Ponzi, or other “get rich quick” schemes; </p>
				<p class="ml-3" >-	are associated with purchases of annuities or lottery contracts, lay-away systems, banking, off-shore banking, foreign exchange, transactions to finance, investing, investment related products, or refinancing debts funded by a credit card;</p>
				<p class="ml-3" >- provide credit repair or debt settlement services; or are prohibited by applicable law.</p>
				
				<p>&nbsp;</p>
				
				<p><strong>NO WAIVER</strong></p>
				<p>No omission or delay on the part of Giltxchange in enforcing any of its rights or powers hereunder shall be deemed to constitute a waiver of any of Giltxchange's rights to exercise such rights and powers and, in any event, shall not constitute or be construed as a continuing waiver.</p>
				<p>&nbsp;</p>
				
				<p><strong>CONTROLLING LAW AND JURISDICTION</strong></p>
				<p>These Terms and Conditions and any action related hereto will be governed by the laws of India without regard to its conflict of laws provisions. The exclusive jurisdiction and venue of any action with respect to the subject matter of these Terms and Conditions will be the courts in New Delhi, India and you hereby waive any objection to jurisdiction and venue in such courts. 
By clicking on the "Submit" button during sign-up, you hereby give your informed consent to the terms and conditions contained herein.</p>
				
			</div>
		</div>
		
		
		
	</div>
	<!--===================== About Working ========================-->
	<div class="about-working" style="display:none;" >
		<div class="container">
			<div class="row">
				<div class="col-xl-5">&nbsp;</div>
				<div class="col-xl-7 col-12">
					<div class="text-head">
						<h5>The Perks</h5>
						<h2 class="h2-main">About working here </h2>
					</div><!--text-head-->
					<ul class="list-unstyled animatedParent">
						<li class="d-flex align-items-center animated bounceInRight delay-250">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about.svg" alt="icon-about">
							<div class="content">
								<h5>Big office</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-500">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about2.svg" alt="icon-about">
							<div class="content">
								<h5>Greate co-workers</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-750">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about3.svg" alt="icon-about">
							<div class="content">
								<h5>Education opportunities</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-1000">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about4.svg" alt="icon-about">
							<div class="content">
								<h5>Soccialy responsible</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-1250">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about5.svg" alt="icon-about">
							<div class="content">
								<h5>Food</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-1250">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about6.svg" alt="icon-about">
							<div class="content">
								<h5>Team spirit</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--===================== End of About Working ========================-->
	<!--===================== Team Slider ========================-->
	<div class="container team-slider-text" style="display:none;" >
		<div class="row">
			<div class="col-xl-12">
				<div class="text-head text-center">
					<h5>Team</h5>
					<h2 class="h2-main">More like a family</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div><!--text-head-->
			</div>
		</div>
	</div>
	<div class="team-slider animatedParent" style="display:none;" >
		<div class="item animated bounceInRight delay-250">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Anna Gross</h5>
				<span>Project Manager</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
		<div class="item animated bounceInRight delay-500">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider2.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Mariya Norman</h5>
				<span>Sales Manager</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin1"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
		<div class="item animated bounceInRight delay-750">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider3.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Sem Smith</h5>
				<span>Director Business Development</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_14" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin5"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
		<div class="item animated bounceInRight delay-1000">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider4.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Rita Marvel</h5>
				<span>Graphic Designer</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_13" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin4"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
		<div class="item animated bounceInRight delay-1250">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider3.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Sem Smith</h5>
				<span>Director Business Development</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_12" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin3"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
	</div>
	<!--===================== End of Team Slider ========================-->
	<!--===================== Join Us ========================-->
	<div class="container join-us" style="display:none;" >
		<div class="row">
			<div class="col-xl-12">
				<div class="text-head text-center">
					<h5>Openings</h5>
					<h2 class="h2-main">Life is short. Join us</h2>
				</div><!--text-head-->
				<!--===================== Join Block ========================-->
				<ul class="animatedParent">
					<li class="d-flex justify-content-between align-items-center animated bounceInUp delay-250">
						<div class="inside">
							<h4>Data Scientist</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<a href="index.html#" class="see-brd-btn">Apply Now</a>
					</li>
					<li class="d-flex justify-content-between align-items-center animated bounceInUp delay-500">
						<div class="inside">
							<h4>Didgital Designer</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<a href="index.html#" class="see-brd-btn">Apply Now</a>
					</li>
					<li class="d-flex justify-content-between align-items-center animated bounceInUp delay-750">
						<div class="inside">
							<h4>Illustrator</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<a href="index.html#" class="see-brd-btn">Apply Now</a>
					</li>
					<li class="d-flex justify-content-between align-items-center animated bounceInUp delay-1000">
						<div class="inside">
							<h4>Data Scientist</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<a href="index.html#" class="see-brd-btn">Apply Now</a>
					</li>
				</ul>
				<!--===================== End of Join Block ========================-->
			</div>
		</div>
	</div>
	<!--===================== End of Join Us ========================-->
@endsection()
@section('footerjs')
    
    
@endsection()
