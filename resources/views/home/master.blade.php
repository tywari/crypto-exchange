<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Giltxchange - Home Page</title>
	<link rel="shortcut icon" type="image/png" href="{{asset('/public/gilt/img/favicon.png')}}"/>
	
	<!-- Appple Touch Icons -->
	<link rel="apple-touch-icon" sizes="57x57" href="{{asset('/public/gilt/img/apple-touch-icon-57x57.png')}}"/>
	<link rel="apple-touch-icon" sizes="72x72" href="{{asset('/public/gilt/img/apple-touch-icon-72x72.png')}}"/>
	<link rel="apple-touch-icon" sizes="114x114" href="{{asset('/public/gilt/img/apple-touch-icon-114x114.png')}}"/>
	<link rel="apple-touch-icon" sizes="144x144" href="{{asset('/public/gilt/img/apple-touch-icon-144x144.png')}}"/>


    <!-- Bootstrap core CSS -->
    <link href="{{asset('/public/gilt/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
     <link href="{{asset('/public/gilt/css/half-slider.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/style.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/media-query.css')}}" rel="stylesheet">
	 <!-- Custom styles for font awsome this template -->
	 <link href="{{asset('/public/gilt/css/brands.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/fa-brands.min.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/fa-regular.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/fa-regular.min.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/fa-solid.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/fa-solid.min.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/fontawesome.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/fontawesome.min.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/fontawesome-all.css')}}" rel="stylesheet">
	 <link href="{{asset('/public/gilt/css/fontawesome-all.min.css')}}" rel="stylesheet">
      <link href="{{asset('/public/css/plugins/ladda/ladda-themeless.min.css')}}" rel="stylesheet">

      <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
      <script>
          var bu = '{{url('/')}}';
      </script>
      @yield('headerjs')
  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-darkblue fixed-top header-menu">
      <div class="container-fluid">
        <a class="navbar-brand" href="#"><img src="{{asset('/public/gilt/img/logo.png')}}"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
		<ul class="navbar-nav ml-auto logo-menu">
            <li class="nav-item first">
              <a class="nav-link" href="#">Exchange</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Info</a>
            </li>            
          </ul>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item first">
              <a class="nav-link" href="#">Login/Register</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">News</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
            <li class="nav-item dropdown last">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                English
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item first" href="#">English 01</a>
                <a class="dropdown-item" href="$">English 02</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>

   	@yield('content')

  <!-- /.container -->
  </footer>
    <div class="darkes-footer">
      <div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6">
				<p>Copyright &copy; <span class="logo-yellow">Giltxchange</span> 2018 All Right Reserved.</p>
			</div>
			<div class="col-sm-12 col-md-6">
				<div class="footer-social">
					<ul>
						<li><a href="https://www.facebook.com/Giltxchangeofficial" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="https://t.me/giltxchange" target="_blank"><i class="fab fa-telegram-plane"></i></a></li>
						<li><a href="https://twitter.com/giltxchange" target="_blank"><i class="fab fa-twitter"></i></a></li>
						<li><a href="https://www.linkedin.com/company/giltxchange/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
						<li><a href="https://medium.com/@giltxchange" target="_blank"><i class="fab fa-medium-m"></i></a></li>						
					</ul>
				</div>
			</div>
		</div>
	</div>
	</div>
    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('/public/gilt/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/public/gilt/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Ladda -->
    <script src="{{asset('/public/js/plugins/ladda/spin.min.js')}}"></script>
    <script src="{{asset('/public/js/plugins/ladda/ladda.min.js')}}"></script>
    <script src="{{asset('/public/js/plugins/ladda/ladda.jquery.min.js')}}"></script>
    @yield('footerjs')

  </body>

</html>

